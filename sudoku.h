#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>
#include <sstream>

#include <QPoint>
#include <QDebug>

class Cell
{
public:
        Cell(int _number, bool _visible = true, int _value = 0) : number(_number),
                                                              value(_value),
                                                              visible(_visible) {}
    int number; // значение клетки
    int value; // введенное значение
    bool visible; // видна ли клетка для пользователя
};

enum Complexity {
                    Easy = 40,
                    Medium = 45,
                    Hard = 50
                };

class Sudoku
{
public:
    Sudoku(Complexity comp);

    void putValue(int x, int y, int value);
    int getNumber(int x, int y);
    int getValue(int x, int y);
    
    QPoint checkCells();
    bool fullCheckCells();

    void printSelf();
    void printSelfFull();
    static const int size = 9;
private:
    Sudoku();
    void generator();
    void mix();

    void transpose();
    void swap_rows_district();
    void swap_colums_district();
    void swap_rows_area();
    void swap_colums_area();

    
    void hideSomeCells();
    
    QPoint checkRows();
    QPoint checkColumns();
    QPoint checkDistrict();

    bool allFilled();

    static const int shuffle_count = 43;
    static const int shuffle_func_count = 5;
    Complexity complexity;
    std::vector< std::vector<Cell> > area;

};

#endif
