#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QVector>
#include <QMessageBox>


#include <algorithm>
#include <cstddef>

#include "sudoku.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    QVector< QVector<QWidget *> > area;

    Sudoku *game;
    bool error;

    void newSudoku(); // Новая игра
    void showSudoku(); // Заполнение формы
    void clearArea();

    QVector<QString> colors; // Хранение цветов
private slots:
    void on_newGameButton_clicked();

    void on_checkButton_clicked();

    void remove_error(QString text);

private:
    Ui::MainWindow *ui;
};

#endif
