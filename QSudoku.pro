#-------------------------------------------------
#
# Project created by QtCreator 2015-06-04T01:01:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QSudoku
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sudoku.cpp

HEADERS  += mainwindow.h \
    sudoku.h

FORMS    += mainwindow.ui
