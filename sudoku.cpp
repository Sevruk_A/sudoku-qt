#include "sudoku.h"

Sudoku::Sudoku(Complexity comp)
{
    complexity = comp; // запоминаем сложность
    generator(); // генерация
    mix();  // сортировка
    hideSomeCells(); // скрытые ячейки (зависит от сложности)
}

void Sudoku::printSelf()
{
    std::cout << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (area[i][j].visible)
                std::cout << area[i][j].number;
            else
                std::cout << " ";
            std::cout << " ";
        }
        std::cout << std::endl;
    }
}
void Sudoku::printSelfFull()
{
    std::cout << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (area[i][j].visible)
                std::cout << area[i][j].number;
            else
                std::cout << area[i][j].value;
            std::cout << " ";
        }
        std::cout << std::endl;
    }
}

void Sudoku::generator()
{
    int n = 3;
    area.resize(size); // задаем количество строк в двумерном массиве
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            area[i].push_back(Cell((i * n + i / n + j) % // помещаем в массив
                                    size + 1));          // новую ячейку с вычисленным знасчением
        }
    }
}

void Sudoku::hideSomeCells()
{
    std::srand(std::time(0));
    int hides = complexity;
    while (hides > 0) {
        // если ячейка со случайными координатами не скрыта
        int i = rand() % 9, j = rand() % 9;
        if (area[i][j].visible) {
            area[i][j].visible = false; // скрываем ее
            hides--;
        }
    }
}

QPoint Sudoku::checkCells()
{
    QPoint row = checkRows(),
    col = checkColumns();
    if (row.x() > -1) // если в строке есть ошибка (>-1)
        return row;
    if (col.y() > -1) // если в столбце есть ошибка
        return col;
    return checkDistrict(); // если в строках и столбцах нет ошибок, возвращаем результат проверки регионов
}

bool Sudoku::fullCheckCells()
{
    QPoint res = checkCells();
    return (res.x() + res.y() < -1) && allFilled();
}

bool Sudoku::allFilled()
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            // если ячейка для ввода (!.visible) и значение == 0
            if (!area[i][j].visible && area[i][j].value == 0)
                return false; // то поле не заполнено
        }
    }
    return true;
}

QPoint Sudoku::checkDistrict()
{
    QString square;
    for (int sq = 0; sq < size; sq++) {
        for (int i = 0; i < 3; i++) {
            int x = (sq / 3) * 3 + i;
            for (int j = 0; j < 3; j++) {
                int y = (sq % 3) * 3 + j;
                if (area[x][y].visible) {
                    if (square.indexOf(QString::number(area[x][y].number)) >= 0)
                    {
                        return QPoint(x - i, y - j);
                    } else {
                        square.append(QString::number(area[x][y].number));
                    }
                } else if (area[x][y].value != 0) {
                    if (square.indexOf(QString::number(area[x][y].value)) >= 0) { //
                        return QPoint(x - i, y - j);
                    } else {
                        square.append(QString::number(area[x][y].value));
                    }
                }
            }
        }
        square.clear();
    }
    return QPoint(-1, -1);
}

QPoint Sudoku::checkRows()
{
    
    QString row;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (area[i][j].visible) {
                if (row.indexOf(QString::number(area[i][j].number)) >= 0) {
                    return QPoint(i, -1);
                } else {
                    row.append(QString::number(area[i][j].number));
                }
            } else if (area[i][j].value != 0) {
                if (row.indexOf(QString::number(area[i][j].value)) >= 0) {
                    return QPoint(i, -1);
                } else {
                    row.append(QString::number(area[i][j].value));
                }
            }
        }
        row.clear();
    }
    return QPoint(-1, -1);
}

QPoint Sudoku::checkColumns()
{
    QString col;

    for (int j = 0; j < size; j++) {
        for (int i = 0; i < size; i++) {
            if (area[i][j].visible) {
                if (col.indexOf(QString::number(area[i][j].number)) >= 0) {
                    return QPoint(-1, j);
                } else {
                    col.append(QString::number(area[i][j].number));
                }
            } else if (area[i][j].value != 0) {
                if (col.indexOf(QString::number(area[i][j].value)) >= 0) {
                    return QPoint(-1, j);
                } else {
                    col.append(QString::number(area[i][j].value));
                }
            }
        }
        col.clear();
    }
    return QPoint(-1, -1);
}

void Sudoku::putValue(int x, int y, int value)
{
    if (!area[x][y].visible)
        area[x][y].value = value;
}

int Sudoku::getNumber(int x, int y)
{
    return area[x][y].visible ? area[x][y].number : 0;
}

int Sudoku::getValue(int x, int y)
{
    return  area[x][y].visible ? 0 : area[x][y].value;
}

void Sudoku::mix()
{
    int i = 0;
    srand( time(0) );
    while (i < shuffle_count) {
        int func_num = rand() % shuffle_func_count;
        switch (func_num) {
        case 0:
            transpose();
            break;
        case 1:
            swap_rows_district();
            break;
        case 2:
            swap_colums_district();
            break;
        case 3:
            swap_rows_area();
            break;
        case 4:
            swap_colums_area();
            break;
        }
        i++;
    }
}

void Sudoku::transpose()
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (j < i) {
                Cell temp = area[i][j];
                area[i][j] = area[j][i];
                area[j][i] = temp;
            }
        }
    }
}

void Sudoku::swap_rows_district()
{
    int row = (std::rand() % 3) * 3;
    int row1 = std::rand() % 3;
    int row2 = std::rand() % 3;
    while (row1 == row2) {
        row1 = std::rand() % 3;
        row2 = std::rand() % 3;
    }
    std::vector<Cell> temp = area[row + row1];
    area[row + row1] = area[row + row2];
    area[row + row2] = temp;
}

void Sudoku::swap_colums_district()
{
    int col = (std::rand() % 3) * 3;
    int col1 = std::rand() % 3;
    int col2 = std::rand() % 3;
    while (col1 == col2) {
        col1 = std::rand() % 3;
        col2 = std::rand() % 3;
    }
    for (int i = 0; i < size; i++) {
        Cell temp = area[i][col + col1];
        area[i][col + col1] = area[i][col + col2];
        area[i][col + col2] = temp;
    }
}

void Sudoku::swap_rows_area()
{
    int row1 = (std::rand() % 3) * 3;
    int row2 = (std::rand() % 3) * 3;
    while (row1 == row2) {
        row1 = (std::rand() % 3) * 3;
        row2 = (std::rand() % 3) * 3;
    }
    for (int i = 0; i < size / 3; i++) {
        std::vector<Cell> temp = area[row1 + i];
        area[row1 + i] = area[row2 + i];
        area[row2 + i] = temp;
    }
}

void Sudoku::swap_colums_area()
{
    int col1 = (std::rand() % 3) * 3;
    int col2 = (std::rand() % 3) * 3;
    while (col1 == col2) {
        col1 = (std::rand() % 3) * 3;
        col2 = (std::rand() % 3) * 3;
    }
    for (int j = 0; j < size / 3; j++) {
        for (int i = 0; i < size; i++) {
            Cell temp = area[i][col1 + j];
            area[i][col1 + j] = area[i][col2 + j];
            area[i][col2 + j] = temp;
        }
    }
}
