#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // помещаем цвета для ячеек регионов в массив
    /*colors.push_back("#FF7373");
    colors.push_back("#FFC673");
    colors.push_back("#FFEA73");
    colors.push_back("#FFF273");
    colors.push_back("#689AD3");
    colors.push_back("#AD66D5");
    colors.push_back("#67E667");
    colors.push_back("#5CCCCC");
    colors.push_back("#F26D93");*/

    colors.push_back("#C6CCD5");
    colors.push_back("white");
    colors.push_back("#C6CCD5");
    colors.push_back("white");
    colors.push_back("#C6CCD5");
    colors.push_back("white");
    colors.push_back("#C6CCD5");
    colors.push_back("white");
    colors.push_back("#C6CCD5");

    game = NULL;
    newSudoku();
}

void MainWindow::newSudoku()
{
    Complexity comp;
    switch (ui->comboBox->currentIndex()) {
    case 0:
        comp = ::Easy;
        break;
    case 1:
        comp = ::Medium;
        break;
    case 2:
        comp = ::Hard;
        break;
    }
    if (game != NULL)
        delete game;
    game = new Sudoku(comp);
    error = false;
    showSudoku();
}

void MainWindow::showSudoku()
{

    clearArea();
    area.resize(game->size);
    for (int i = 0; i < game->size; i++) {
        for (int j = 0; j < game->size; j++) {
            if (game->getNumber(i, j) > 0) {
                QLabel *label = new QLabel(QString::number(game->getNumber(i, j)));
                label->setAlignment(Qt::AlignCenter);
                label->setStyleSheet(QString("border: 2px solid grey; ") +
                                     QString("background-color: " + colors[(i / 3) * 3 + j / 3]));
                area[i].push_back(label);
                ui->gridLayout->addWidget(area[i][j], i, j);
            } else {
                QLineEdit *edit = new QLineEdit();
                QRegExp exp("[1-9] {1}");
                QObject::connect(edit, SIGNAL(textChanged(QString)), this, SLOT(remove_error(QString)));
                edit->setAlignment(Qt::AlignCenter);
                edit->setStyleSheet(QString("border: 1px dotted black; color: black; ") +
                                    QString("background-color: " + colors[(i / 3) * 3 + j / 3]));
                edit->setValidator(new QRegExpValidator(exp, this));
                edit->setMaxLength(1);
                area[i].push_back(edit);
                ui->gridLayout->addWidget(area[i][j], i, j);
            }
        }
    }
}

void MainWindow::clearArea()
{
    for (int i = 0; i < area.size(); i++) {
        for (int j = 0; j < area[i].size(); j++)
            delete area[i][j];
        area[i].clear();
    }
}

MainWindow::~MainWindow()
{
    clearArea();
    delete game;
    delete ui;
}

void MainWindow::on_newGameButton_clicked()
{
    newSudoku();
}

void MainWindow::on_checkButton_clicked()
{
    for (int i = 0; i < game->size; i++) {
        for (int j = 0; j < game->size; j++) {
            if (game->getNumber(i, j) == 0 &&
                    static_cast<QLineEdit *>(area[i][j])->text().toInt() > 0) {
                game->putValue(i, j, static_cast<QLineEdit *>(area[i][j])->text().toInt());
            }
        }
    }
    if (game->fullCheckCells()) {
        int res = QMessageBox::information(this, "Поздравляем!",
                                           "Вы прошли игру! Начать заново?",
                                           QMessageBox::Yes | QMessageBox::No);
        if (res == QMessageBox::Yes) {
            delete game;
            newSudoku();
        }
    } else {
        QPoint res = game->checkCells();
        error = true;
        if (res.x() > -1 && res.y() > -1) {
            for (int i = res.x(); i < res.x() + 3; i++) {
                for (int j = res.y(); j < res.y() + 3; j++) {
                    area[i][j]->setStyleSheet("background-color: red");
                }
            }
        } else {
            if (res.x() > -1) {
                for (int j = 0; j < game->size; j++) {
                    area[res.x()][j]->setStyleSheet("background-color: red");
                }
            } else if (res.y() > -1) {
                for (int i = 0; i < game->size; i++) {
                    area[i][res.y()]->setStyleSheet("background-color: red");
                }
            } else {
                error = false;
            }

        }
    }

}

void MainWindow::remove_error(QString text)
{
    if (error) {
        for (int i = 0; i < game->size; i++) {
            for (int j = 0; j < game->size; j++) {
                if (game->getNumber(i, j) > 0) {
                    area[i][j]->setStyleSheet(QString("border: 2px solid grey; ") +
                                         QString("background-color: " + colors[(i / 3) * 3 + j / 3]));
                } else {
                    area[i][j]->setStyleSheet(QString("border: 2px dotted black; color: white; ") +
                                        QString("background-color: " + colors[(i / 3) * 3 + j / 3]));
                }
            }
        }
        error = false;
    }
}
